import { handleFirstResponse } from "../../utils/apiUtils"

export const TranslatorAPI = {
    getTranslator() {
        return fetch("http://localhost:3000/images")
            .then(handleFirstResponse)
    }
}