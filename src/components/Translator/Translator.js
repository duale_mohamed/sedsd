import { useEffect, useState } from "react";
import {Display} from "../Translator/Display"
import styles from "../Translator/Translator.module.css"
import { handleFirstResponse } from "../../utils/apiUtils";
import {Link, Redirect } from "react-router-dom";

const Translator = () => {

  const [ state, setState ] = useState({
    text: "",
    displaySigns: ""
  })

  const handleClick = () => {
    setState({...state, displaySigns: state.text})
    fetch(
      "http://localhost:3000/translations",
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(state.text),
      }
    ).then(handleFirstResponse);
  }

  

  return (
    
          
          
    <div className="translator-container">
      
      <div className="header">
          <nav className="navbar navbar-light bg-light">
            <span className="navbar-brand mb-0 h1">Translator App</span>
            <Link to="/profile">
            <button class="btn btn-outline-dark my-2 my-sm-0" type="submit">Profile
            </button>
            </Link>
          </nav>
          </div>
        <label className="form-label">Words to translate: </label>
        <input className="form-control" type="text" onChange={(e) => setState({...state, text: e.target.value})} value={state.text}></input>
        <button type="button" className="btn btn-primary mb-3 mt-3" onClick={() => handleClick()}>Translate</button>
        <Display letters={[...state.displaySigns]}/>
    </div>
    
  );
};

export default Translator;
