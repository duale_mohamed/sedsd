import { useEffect, useState } from "react";

export const Display = (props) => {
    return (
        <div className="image-container">
            {props.letters.map(l => <img src={`/images/${l}.png`} />)}
        </div>
    )
}