import { handleFirstResponse } from "../../utils/apiUtils";

export const LoginAPI = {
  login(credentials) {
    return fetch(
      "http://localhost:3000/users",
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
        //body: JSON.stringify(credentials),
      }
    ).then(handleFirstResponse);
  },
};
