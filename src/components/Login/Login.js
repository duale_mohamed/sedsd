import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {Link, Redirect } from "react-router-dom";
import AppContainer from "../../hoc/AppContainer";
import { loginAttemptAction } from "../../store/actions/loginActions";

const Login = () => {
  const dispatch = useDispatch();
  const { loginError, loginAttempting } = useSelector(
    (state) => state.loginReducer
  );
  const { loggedin } = useSelector((state) => state.sessionReducer);

  const [credentials, setCredentials] = useState({
    username: "",
    password: "",
  });

  const onInputChange = (event) => {
    setCredentials({
      ...credentials,
      [event.target.id]: event.target.value,
    });
  };

  const onFormSubmit = (event) => {
    event.preventDefault(); //Stops page from reloading -- SPA
    dispatch(loginAttemptAction(credentials));
  };

  return (
    <>
      {loggedin && <Redirect to="/translator" />}
      {!loggedin && (
        <AppContainer>
          <form className="mt-3 mb-3" onSubmit={onFormSubmit}>
            <h1>Login to Translator App</h1>
            <p>Welcome to the translator app 😎</p>

            <div className="mb-3">
              <label htmlFor="" className="form-label">
                Username
              </label>
              <input
                id="username"
                type="text"
                placeholder="Enter your username"
                className="form-control"
                onChange={onInputChange}
              />
            </div>

            <div className="mb-3">
              <label htmlFor="" className="form-label">
                Password
              </label>
              <input
                id="password"
                type="password"
                placeholder="Enter your password"
                className="form-control"
                onChange={onInputChange}
              />
            </div>

            <button type="submit" className="btn btn-primary btn-lg">
              Login
            </button>
          </form>

          {loginAttempting && <p>Trying to login</p>}

          {loginError && (
            <div className="alert alert-danger" role="alert">
              <h4>Unsuccessful</h4>
              <p className="mb-0">{loginError}</p>
            </div>
          )}

            <p className="mb-3">
                <Link to="/register">Dont have account? Register here</Link>
            </p>

        </AppContainer>
      )}
    </>
  );
};

export default Login;
