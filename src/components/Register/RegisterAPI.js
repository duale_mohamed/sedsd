import { handleFirstResponse } from "../../utils/apiUtils"

export const RegisterAPI = {
    register({ username, password}) {
        return fetch("http://localhost:3000/users"), {
            method: 'POST',
            headers: {
                'Content-Type': 'application/JSON'
            },
            body: JSON.stringify({ username, password})
        }
        .then(handleFirstResponse)
    }
}