import { Link, Redirect } from "react-router-dom"
import AppContainer from "../../hoc/AppContainer"
import { useSelector } from "react-redux"
import { useState } from "react"

const Register = () => {

    const { loggedin } = useSelector(state => state.sessionReducer)

    const [ user, setUser] = useState({
        username: "",
        password: "",
        confirmPassword: "",
    })

    const onRegisterSubmit = event => {
        event.preventDefault()
        console.log('Register.onRegisterSubmit()',user)
    }

    const onInputChange = event => {
        setUser({
            ...user,
            [event.target.id]: event.target.value
        })
    }

    return  (
        <AppContainer>
            { loggedin && <Redirect to="/translator" />}
            <form>
                <h1>Register Translator</h1>
                <p>Complete the form to create your account</p>

                <div className="mt-3 mb-3" onSubmit={ onRegisterSubmit }>
                    <label htmlFor="username" className="form-label">Choose username: *</label>
                    <input onChange={ onInputChange } type="text" id="username" className="form-control" placeholder="Michael Jackson"/>
                </div>

                <div className="mb-3">
                <label htmlFor="password" className="form-label">Choose password: *</label>
                    <input onChange={ onInputChange } type="text" id="password" className="form-control" placeholder="******"/>
                </div>

                <div className="mb-3">
                <label htmlFor="confirmPassword" className="form-label">Confirm password: *</label>
                    <input onChange={ onInputChange } type="text" id="confirmPassword" className="form-control" placeholder="******"/>
                </div>

                <button className="btn btn-success btn-lg">Register</button>
            </form>

            <p className="mb-3">
                <Link to="/">Already registered? Login here</Link>
            </p>
        </AppContainer>
    )
}
export default Register