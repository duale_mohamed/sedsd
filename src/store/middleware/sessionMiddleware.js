import {
  ACTION_SESSION_INIT,
  ACTION_SESSION_SET,
  sessionSetAction,
} from "../actions/sessionAction";

export const sessionMiddleware =
  ({ dispatch }) =>
  (next) =>
  (action) => {
    next(action);

    if (action.type === ACTION_SESSION_INIT) {
      const storedSession = localStorage.getItem("translator-ss");

      if (!storedSession) {
        return;
      }
      const session = JSON.parse(storedSession);
      dispatch(sessionSetAction(session));
    }

    if (action.type === ACTION_SESSION_SET) {
      //store session
      localStorage.setItem("translator-ss", JSON.stringify(action.payload));
    }
  };
