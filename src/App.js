import "./App.css";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import Login from "./components/Login/Login";
import NotFound from "./components/NotFound/NotFound";
import AppContainer from "./hoc/AppContainer";
import Translator from "./components/Translator/Translator";
import Register from "./components/Register/Register";
import Profile  from "./components/Profile/Profile";


function App() {
  return (
    <BrowserRouter>
      <AppContainer>
        
          <Switch>
            <Route path="/" exact component={Login}></Route>
            <Route path="/register" component={Register}></Route>
            <Route path="/translator" component={Translator}></Route>
            <Route path="/profile" component={Profile}></Route>
            <Route path="*" component={NotFound}></Route>
          </Switch>
      </AppContainer>
    </BrowserRouter>
  );
}

export default App;
