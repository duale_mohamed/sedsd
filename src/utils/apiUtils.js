export const handleFirstResponse = async response => {
    if (!response.ok) {
        const { error = 'Unkown error occurred.' } = await response.json()
        throw new Error(error)
    }

    return response.json()
}